import uvicorn
from fastapi import FastAPI

from src.routers.router_city import routers as router_city
from src.routers.router_picnic import routers as router_picnic
from src.routers.router_user import routers as router_user
from src.routers.router_registration import routers as router_registration
from src.connectors.postgres_connector import PostgresConnector
from src.connectors.http_connector import AsyncHttpConnector
from src.config import PostgresConfig, HTTPConfig
from src.logger.logger import AsyncLogger

app = FastAPI()

# Регистрация маршрутов
app.include_router(router_city)
app.include_router(router_picnic)
app.include_router(router_user)
app.include_router(router_registration)


@app.on_event("startup")
async def startup():
    app.state.transaction_map = {}
    app.state._db = PostgresConnector()
    # При соединении с базой создай все движки с необходимыми уровнями изоляции,
    # По умолчанию создается движок с уровнем изоляции autocommit
    app.state._db.connect(user=PostgresConfig.PG_USER,
                          password=PostgresConfig.PG_PASSWORD,
                          host=PostgresConfig.PG_HOST,
                          port=PostgresConfig.PG_PORT,
                          database_name=PostgresConfig.PG_DATABASE_NAME,
                          repeatable_read_engine=True)

    app.state._http = AsyncHttpConnector()
    # Так как разработчики aiohttp не рекомендуют создавать сессию на каждый запрос,
    # будем переиспользовать сессии, получая соединения из набора для большей производительности
    app.state._http.create_session(HTTPConfig.WEATHER_SESSION_NAME)
    app.state._logger = AsyncLogger().create()


@app.on_event("shutdown")
async def shutdown_event():
    """Сброс соединений"""
    await app.state._db.disconnect()
    await app.state._http.close_all_sessions()


def run():
    uvicorn.run("main:app", host="0.0.0.0")


if __name__ == "__main__":
    run()
