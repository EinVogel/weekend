# Запуск приложения для демонстрации
1) Убедитесь, что установлен make, docker, docker compose
2) git clone https://gitlab.com/EinVogel/weekend.git
3) cd weekend
4) make build
5) make start
6) make test
7) Перейти по адресу 
   http://0.0.0.0:8000/docs

# Архитектура папок проекта
 - src - основной код проекта
   - connectors - внешние соединения
   - dependencies - зависимости сервисов
   - exceptions - исключения, которые пробрасываем при отлове ошибок
   - exceptions -  логгер сервиса
   - migration -  миграции
   - models - модели
   - routers - маршруты к сервисам
   - schemas - схемы входных выходных данных маршрутов
   - serializers - сложные сериализации данных
   - services - сервисы, которые вызываются маршрутами
 - tests - тесты