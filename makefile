build:
	cp src/weekend_local_settings.txt src/weekend_local_settings.env; docker-compose --env-file src/weekend_local_settings.env build

start:
	 docker-compose --env-file src/weekend_local_settings.env up

test:
	docker-compose exec backend_weekend poetry run python -m pytest --cov=src tests

