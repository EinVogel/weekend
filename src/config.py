import os


class HTTPConfig:
    """Настройки для http соединений"""
    WEATHER_SESSION_NAME = 'weather_session'


class WeatherConfig:
    """Настройки получения погоды"""
    WEATHER_ENDPOINT = os.getenv("WEATHER_ENDPOINT")
    WEATHER_API_KEY = os.getenv("WEATHER_API_KEY")
    WEATHER_TIMEOUT = int(os.getenv("WEATHER_TIMEOUT"))


class PostgresConfig:
    """Настройки для соединения с postgres"""
    PG_USER = os.getenv("PG_USER")
    PG_PASSWORD = os.getenv("PG_PASSWORD")
    PG_HOST = os.getenv("PG_HOST")
    PG_PORT = os.getenv("PG_PORT")
    PG_DATABASE_NAME = os.getenv("PG_DATABASE_NAME")
    CITY_TABLE_NAME = os.getenv("CITY_TABLE_NAME")
    PICNIC_TABLE_NAME = os.getenv("PICNIC_TABLE_NAME")
    REGISTRATION_TABLE_NAME = os.getenv("REGISTRATION_TABLE_NAME")
    USER_TABLE_NAME = os.getenv("USER_TABLE_NAME")


class LoggerConfig:
    """Настройки логгера"""
    LOG_FILE_PATH = os.getenv("LOG_FILE_PATH")

