from aiologger.handlers.files import AsyncFileHandler
from aiologger.logger import Logger
from aiologger.levels import LogLevel
from aiologger.formatters.base import Formatter

from src.config import LoggerConfig


class AsyncLogger:

    def create(self):
        fmt = '%(asctime)s - [%(levelname)s] -  %(name)s -' \
              ' file:%(filename)s - func:%(funcName)s - line:%(lineno)d - message: %(message)s'
        formatter = Formatter(fmt=fmt)
        # настройка логгера
        logger = Logger.with_default_handlers(
            level=LogLevel.INFO,
            formatter=formatter
        )

        # настройка обработчика (handler) для записи в файл
        file_handler = AsyncFileHandler(filename=LoggerConfig.LOG_FILE_PATH)
        file_handler.formatter = formatter
        logger.add_handler(file_handler)
        return logger
