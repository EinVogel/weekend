from src.connectors.http_connector import AsyncHttpConnector
from starlette.requests import Request


def get_http_connector(request: Request) -> AsyncHttpConnector:
    return request.app.state._http
