from typing import Callable, Type
from fastapi import Depends
from aiologger.loggers.json import JsonLogger

from src.connectors.postgres_connector import PostgresConnector
from src.connectors.http_connector import AsyncHttpConnector
from src.services.service_base import BaseService
from src.dependencies.database import get_database_connector
from src.dependencies.http import get_http_connector
from src.dependencies.log import get_logger


def get_service(service_type: Type[BaseService]) -> Callable:
    def get_service_with_dependencies(db: PostgresConnector = Depends(get_database_connector),
                                      http: AsyncHttpConnector = Depends(get_http_connector),
                                      logger: JsonLogger = Depends(get_logger)) -> BaseService:
        """Получение всех необходимых зависимостей"""
        return service_type(db, http, logger)
    return get_service_with_dependencies
