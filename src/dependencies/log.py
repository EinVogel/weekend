from starlette.requests import Request
from aiologger.loggers.json import JsonLogger


def get_logger(request: Request) -> JsonLogger:
    return request.app.state._logger
