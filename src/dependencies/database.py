from src.connectors.postgres_connector import PostgresConnector
from starlette.requests import Request


def get_database_connector(request: Request) -> PostgresConnector:
    return request.app.state._db
