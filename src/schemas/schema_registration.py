from pydantic import BaseModel


class NewRegistrationSchema(BaseModel):
    """Схема новой регистрации пользователей на пикник"""
    user_id: int
    picnic_id: int


class RegistrationSchema(BaseModel):
    """Схема регистрации пользователей на пикник"""
    id: int
    user_id: int
    picnic_id: int

