from datetime import datetime
from pydantic import BaseModel
from typing import List

from src.schemas.schema_user import UserSchema


class NewPicnicSchema(BaseModel):
    """Схема нового пикника"""
    city_id: int
    time: datetime


class PicnicSchema(BaseModel):
    """Схема пикника"""
    id: int
    city: str
    time: datetime


class PicnicWithUsersSchema(PicnicSchema):
    """Схема пикника с пользователями"""
    users: List[UserSchema]
