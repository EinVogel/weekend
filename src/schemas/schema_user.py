from pydantic import BaseModel


class NewUserSchema(BaseModel):
    """Схема нового пользователя"""
    name: str
    surname: str
    age: int


class UserSchema(BaseModel):
    """Схема пользователя"""
    id: int
    name: str
    surname: str
    age: int






