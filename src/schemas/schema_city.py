from pydantic import BaseModel


class NewCitySchema(BaseModel):
    """Схема нового города"""
    name: str

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = kwargs.get('name').capitalize()


class CitySchema(BaseModel):
    """Схема города"""
    id: int
    name: str
    weather: float







