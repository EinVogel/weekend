from fastapi import APIRouter, Depends, status
from typing import Optional, List
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from src import exceptions
from src.schemas.schema_city import NewCitySchema, CitySchema
from src.services.service_city import CityService
from src.dependencies.base import get_service

routers = APIRouter()


@routers.post('/city', tags=['Города'], response_model=CitySchema)
async def create_city(new_city: NewCitySchema,
                      service_city: CityService = Depends(get_service(CityService))):
    """Создание города по его названию"""
    try:
        city = await service_city.create_city(new_city)
        json_compatible_item_data = jsonable_encoder(city.__dict__)
        await service_city.logger.info("Город успешно создан")
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=json_compatible_item_data)
    except exceptions.CityException as ex:
        response_data = ex.data()
        status_code = status.HTTP_400_BAD_REQUEST
        json_compatible_item_data = jsonable_encoder(response_data)
        await service_city.logger.error(f"Ошибка создания города: {ex.data()}")
        return JSONResponse(status_code=status_code, content=json_compatible_item_data)


@routers.get('/city', tags=['Города'], response_model=List[CitySchema])
async def get_city(name: Optional[str] = None,
                   service_city: CityService = Depends(get_service(CityService))):
    """ Получение списка городов """
    try:
        cities = await service_city.get_city(name)
        json_compatible_item_data = jsonable_encoder(cities)
        await service_city.logger.info("Города успешно получены")
        return JSONResponse(status_code=status.HTTP_200_OK, content=json_compatible_item_data)
    except (exceptions.CityException, exceptions.WeatherException) as ex:
        response_data = ex.data()
        status_code = status.HTTP_404_NOT_FOUND
        json_compatible_item_data = jsonable_encoder(response_data)
        await service_city.logger.error(f"Ошибка получения городов: {ex.data()}")
        return JSONResponse(status_code=status_code, content=json_compatible_item_data)

