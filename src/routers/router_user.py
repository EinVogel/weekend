from fastapi import APIRouter, Depends, status
from typing import List, Optional
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from src import exceptions
from src.services.service_user import UserService
from src.schemas.schema_user import NewUserSchema, UserSchema
from src.dependencies.base import get_service

routers = APIRouter()


@routers.post('/user', tags=['Пользователи'], response_model=UserSchema)
async def create_user(new_user: NewUserSchema,
                      service_user: UserService = Depends(get_service(UserService))):
    """Регистрация пользователя"""
    try:
        user = await service_user.create_user(new_user)
        json_compatible_item_data = jsonable_encoder(user.__dict__)
        await service_user.logger.info("Пользователь успешно создан")
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=json_compatible_item_data)
    except exceptions.UserException as ex:
        response_data = ex.data()
        status_code = status.HTTP_400_BAD_REQUEST
        json_compatible_item_data = jsonable_encoder(response_data)
        await service_user.logger.error(f"Ошибка создания пользователя: {ex.data()}")
        return JSONResponse(status_code=status_code, content=json_compatible_item_data)


@routers.get('/user', tags=['Пользователи'], response_model=List[UserSchema])
async def get_user(name: Optional[str] = None,
                   surname: Optional[str] = None,
                   service_user: UserService = Depends(get_service(UserService))):
    """Список пользователей"""
    try:
        users = await service_user.get_users(name, surname)
        json_compatible_item_data = jsonable_encoder(users)
        await service_user.logger.info("Успешно получены пользователи")
        return JSONResponse(status_code=status.HTTP_200_OK, content=json_compatible_item_data)
    except exceptions.UserException as ex:
        response_data = ex.data()
        status_code = status.HTTP_404_NOT_FOUND
        json_compatible_item_data = jsonable_encoder(response_data)
        await service_user.logger.error(f"Ошибка создания пользователя: {ex.data()}")
        return JSONResponse(status_code=status_code, content=json_compatible_item_data)
