from fastapi import APIRouter, Depends, status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from src import exceptions
from src.schemas.schema_registration import NewRegistrationSchema, RegistrationSchema
from src.services.service_registration import RegistrationService
from src.dependencies.base import get_service

routers = APIRouter()


@routers.post('/registration', tags=['Регистрация на пикник'], response_model=RegistrationSchema)
async def registration_to_picnic(new_registration: NewRegistrationSchema,
                                 service_registration: RegistrationService = Depends(get_service(RegistrationService))):
    """Регистрация пользователя на пикник"""
    try:
        user = await service_registration.create_registration(new_registration)
        json_compatible_item_data = jsonable_encoder(user.__dict__)
        await service_registration.logger.info(f"Пользователь с id {new_registration.user_id}"
                                               f"успешно зарегистрирован")
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=json_compatible_item_data)
    except exceptions.RegistrationException as ex:
        response_data = ex.data()
        status_code = status.HTTP_400_BAD_REQUEST
        json_compatible_item_data = jsonable_encoder(response_data)
        await service_registration.logger.error(f"Ошибка регистрации пользователя с"
                                                f" id {new_registration.user_id}: {ex.data()}")
        return JSONResponse(status_code=status_code, content=json_compatible_item_data)
