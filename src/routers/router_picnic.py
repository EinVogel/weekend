import datetime as dt
from fastapi import APIRouter, Query, Depends, status
from typing import List
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from src import exceptions
from src.schemas.schema_picnic import NewPicnicSchema, PicnicSchema, PicnicWithUsersSchema
from src.services.service_picnic import PicnicService
from src.dependencies.base import get_service

routers = APIRouter()


@routers.post('/picnic', tags=['Пикники'], response_model=PicnicSchema)
async def create_picnic(new_picnic: NewPicnicSchema,
                        service_picnic: PicnicService = Depends(get_service(PicnicService))):
    """Создание пикников"""
    try:
        picnic = await service_picnic.create_picnic(new_picnic)
        json_compatible_item_data = jsonable_encoder(picnic.__dict__)
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=json_compatible_item_data)
    except exceptions.PicnicException as ex:
        response_data = ex.data()
        status_code = status.HTTP_400_NOT_FOUND
        json_compatible_item_data = jsonable_encoder(response_data)
        await service_picnic.logger.error(f"Ошибка создания пикника: {ex.data()}")
        return JSONResponse(status_code=status_code, content=json_compatible_item_data)


@routers.get('/picnic', tags=['Пикники'], response_model=List[PicnicWithUsersSchema])
async def get_picnic(datetime: dt.datetime = Query(default=None, description='Время пикника (по умолчанию не задано)'),
                     past: bool = Query(default=True, description='Включая уже прошедшие пикники'),
                     service_picnic: PicnicService = Depends(get_service(PicnicService))):
    """Получение списка пикников"""

    try:
        picnics = await service_picnic.get_picnic(datetime, past)
        json_compatible_item_data = jsonable_encoder(picnics)
        await service_picnic.logger.info(f"Пикник успешно получен")
        return JSONResponse(status_code=status.HTTP_200_OK, content=json_compatible_item_data)
    except exceptions.PicnicException as ex:
        response_data = ex.data()
        status_code = status.HTTP_404_NOT_FOUND
        json_compatible_item_data = jsonable_encoder(response_data)
        await service_picnic.logger.error(f"Ошибка при получении пикников: {ex.data()}")
        return JSONResponse(status_code=status_code, content=json_compatible_item_data)
