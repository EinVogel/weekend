from typing import List, Tuple

from src.schemas.schema_picnic import PicnicWithUsersSchema
from src.schemas.schema_user import UserSchema
from src.models import Picnic, City, User


class PicnicSerializer:
    """Класс сериализатор пикника"""

    def serialize_picnic(self, picnics: List[Tuple[Picnic, City, User]]) -> List[PicnicWithUsersSchema]:
        """Функция сериализвации выходных данных пикника"""
        picnics_id = {}
        picnics_data = []
        for registration in picnics:
            picnic, city, _ = registration
            if picnics_id.get(picnic.id):
                continue
            picnics_id[picnic.id] = True
            picnic_with_users = PicnicWithUsersSchema(**{'id': picnic.id,
                                                         'city': city.name,
                                                         'time': picnic.time,
                                                         'users': []})
            for picnic_for_this_user, _, user in picnics:
                if picnic.id == picnic_for_this_user.id and user:
                    if UserSchema(**user.__dict__) not in picnic_with_users.users:
                        picnic_with_users.users.append(UserSchema(**user.__dict__))
            picnics_data.append(picnic_with_users)

        return picnics_data
