from src.exceptions.exceptions_main_app import MainAppException


class PicnicException(MainAppException):
    """Базовое исключение пикника"""


class NotFoundPicnicException(PicnicException):
    """Исключение, выпадающее при отсутствии пикника в базе данных"""
    status = "NOT_FOUND_PICNIC_ERROR"
    message = "Не найдено в базе пикников с запрашиваемыми идентификаторами"


class CreatePicnicException(PicnicException):
    """Исключение, выпадающее при ошибке создания пикника"""
    status = "CREATE_PICNIC_ERROR"
    message = "Ошибка создания пикника в базе"
