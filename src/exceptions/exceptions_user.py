from src.exceptions.exceptions_main_app import MainAppException


class UserException(MainAppException):
    """Базовое исключение пользователя"""


class CreateUserException(UserException):
    """Исключение вызываемое в случае неуспешного создания пользователя"""
    status = "CREATE_USER_ERROR"
    message = "Ошибка при регистрации пользователя"


class NotFoundUsersException(UserException):
    """Исключение, выпадающее при отсутствии необходимых пользователей в базе данных"""
    status = "NOT_FOUND_USER_ERROR"
    message = "Не найдено в базе пользователей с запрашиваемыми идентификаторами"

