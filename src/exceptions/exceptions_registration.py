from src.exceptions.exceptions_main_app import MainAppException


class RegistrationException(MainAppException):
    """Базовое исключение регистрации пользователя на пикник"""


class CreateRegistrationException(RegistrationException):
    """Исключение вызываемое в случае неуспешной регистрации на пикник"""
    status = "CREATE_REGISTRATION_ERROR"
    message = "Ошибка при регистрации пользователя"


class ExistRegistrationException(RegistrationException):
    """Исключение вызываемое в случае повторной попытки регистрации пользователя"""
    status = "USER_IS_ALREADY_REGISTERED_ERROR"
    message = "Пользователь уже зарегистрирован"

