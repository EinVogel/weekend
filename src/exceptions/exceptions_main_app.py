class MainAppException(Exception):
    status: str
    message: str

    @classmethod
    def data(cls):
        return {"status": cls.status,
                "message": cls.message}
