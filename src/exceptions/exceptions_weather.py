from src.exceptions.exceptions_main_app import MainAppException


class WeatherException(MainAppException):
    """Базовое исключение погоды"""


class NotFoundWeatherException(WeatherException):
    """Исключение, выпадающее при недоступности сервиса погоды"""
    status = "NOT_FOUND_WEATHER_ERROR"
    message = "Не удалось определить погоду"
