from src.exceptions.exceptions_main_app import MainAppException


class CityException(MainAppException):
    """Базовое исключение города"""


class NotExistCityException(CityException):
    """Исключение, выпадающее при добавлении несуществующего города"""
    status = "NOT_EXIST_CITY_ERROR"
    message = "Параметр name должен быть существующим городом"


class NotFoundWeatherException(CityException):
    """Исключение, выпадающее при недоступности сервиса погоды"""
    status = "NOT_FOUND_WEATHER_ERROR"
    message = "Не удалось определить погоду"


class NotFoundCityException(CityException):
    """Исключение, выпадающее при отсутствии города в базе данных"""
    status = "NOT_FOUND_CITY_ERROR"
    message = "Не найдено в базе городов с запрашиваемыми идентификаторами"


class CreateCityException(CityException):
    """Исключение, выпадающее при ошибке создания города"""
    status = "CREATE_CITY_ERROR"
    message = "Ошибка создания города в базе данных"
