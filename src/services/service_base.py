from src.connectors.postgres_connector import PostgresConnector
from src.connectors.http_connector import AsyncHttpConnector
from aiologger.loggers.json import JsonLogger


class BaseService:
    def __init__(self,
                 db: PostgresConnector,
                 http: AsyncHttpConnector,
                 logger: JsonLogger) -> None:
        self.db = db
        self.http = http
        self.logger = logger
