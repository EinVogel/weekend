import asyncio
from sqlalchemy import insert, select
from typing import List, Optional

from src import exceptions
from src.services.service_base import BaseService
from src.models import City
from src.schemas.schema_city import NewCitySchema, CitySchema
from src.config import WeatherConfig, HTTPConfig


class CityService(BaseService):

    def get_weather_url(self, city: str) -> str:
        """ Генерирует url включая в него необходимые параметры """
        url = WeatherConfig.WEATHER_ENDPOINT
        url += '?units=metric'
        url += '&q=' + city
        url += '&appid=' + WeatherConfig.WEATHER_API_KEY
        return url

    async def get_weather(self, city: str) -> Optional[dict]:
        """
        Возвращает текущую погоду в этом городе
        """
        url = self.get_weather_url(city)
        try:
            response = await self.http.send_request(method='GET',
                                                    url=url,
                                                    session_name=HTTPConfig.WEATHER_SESSION_NAME,
                                                    ssl_verify=False,
                                                    timeout=WeatherConfig.WEATHER_TIMEOUT)
        except asyncio.exceptions.TimeoutError:
            # Если запрос отвалился по таймауту
            return None
        if response.status == 404:
            # Если такого города не существует
            raise exceptions.NotExistCityException()
        data = await response.json()
        return {city: data['main']['temp']}

    async def get_weather_in_many_cities(self, cities: list) -> list:
        tasks = []
        for city in cities:
            tasks.append(self.get_weather(city))
        # TODO добавить тайм ауты на получение и ретраи
        weather_data_list = await asyncio.gather(*tasks, return_exceptions=True)
        return list(weather_data_list)

    async def create_city(self, new_city: NewCitySchema) -> CitySchema:
        """Создание города в базе"""
        weather_data = await self.get_weather(new_city.name)
        session = self.db.get_session()
        try:
            async with session() as session:
                exist_city_query = select(City).where(City.name == new_city.name)
                city_cursor = await session.execute(exist_city_query)
                city = city_cursor.fetchone()
                if not city:
                    new_city_query = insert(City).values(new_city.dict()).returning(City.id,
                                                                                    City.name)

                    new_city = await session.execute(new_city_query)
                    await session.commit()
                    city = new_city.fetchone()
                    return CitySchema(id=city[0], name=city[1], weather=weather_data[city[1]])
            return CitySchema(id=city.id, name=city.name, weather=weather_data[city.name])
        except BaseException:
            raise exceptions.CreateCityException()

    async def get_city(self, name: str) -> List[CitySchema]:
        """Получение города из базы"""
        select_params = {}
        if name:
            select_params.update({'name': name.strip().capitalize()})
        session = self.db.get_session()
        async with session() as session:
            select_city_query = select(City).filter_by(**select_params)
            city_cursor = await session.execute(select_city_query)
            cities = city_cursor.fetchall()
            if not cities:
                raise exceptions.NotFoundCityException()
        weather_data_list = await self.get_weather_in_many_cities([city[0].name for city in cities])
        # Проверка на наличие неуспешных запросов погоды
        if None in weather_data_list:
            raise exceptions.NotFoundWeatherException()

        return [CitySchema(id=city[0].id, name=city[0].name, weather=weather_data[city[0].name]) for
                city, weather_data in zip(cities, weather_data_list)]
