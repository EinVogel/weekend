from sqlalchemy import insert, select
from typing import List, Optional
import datetime as dt

from src import exceptions
from src.services.service_base import BaseService
from src.models import Picnic, City, Registration, User
from src.schemas.schema_picnic import NewPicnicSchema, PicnicSchema, PicnicWithUsersSchema
from src.serializers.serializer_picnic import PicnicSerializer


class PicnicService(BaseService):

    async def create_picnic(self, new_picnic: NewPicnicSchema) -> PicnicSchema:
        """Создание пикника в базе"""
        try:
            session = self.db.get_session()
            async with session() as session:
                new_picnic_query = insert(Picnic).values(new_picnic.dict()).returning(Picnic.id,
                                                                                      Picnic.city_id,
                                                                                      Picnic.time,
                                                                                      )
                new_picnic_cursor = await session.execute(new_picnic_query)
                picnik = new_picnic_cursor.fetchone()

                select_city_query = select(City.name).where(City.id == new_picnic.city_id)
                city_cursor = await session.execute(select_city_query)
                city = city_cursor.fetchone()

                return PicnicSchema(id=picnik[0],
                                    city=city[0],
                                    time=picnik[2],
                                    users=[]
                                    )
        except BaseException:
            raise exceptions.CreatePicnicException()

    async def get_picnic(self,
                         datetime: dt.datetime,
                         past: bool,
                         name: Optional[str] = None) -> List[PicnicWithUsersSchema]:
        """Получение пикников из базы"""
        session = self.db.get_session()
        filter_params = {}
        if name:
            filter_params = {'name': name.capitalize()}
        async with session() as session:

            select_picnic_query = select(Picnic,
                                         City,
                                         User) \
                .join(City, City.id == Picnic.city_id) \
                .join(Registration, Picnic.id == Registration.picnic_id, isouter=True) \
                .join(User, User.id == Registration.user_id, isouter=True) \
                .filter_by(**filter_params)

            if datetime is not None:
                select_picnic_query = select_picnic_query.filter(Picnic.time == datetime)
            if not past:
                select_picnic_query = select_picnic_query.filter(Picnic.time >= dt.datetime.now())
            picnic_cursor = await session.execute(select_picnic_query)
            if not picnic_cursor:
                raise exceptions.NotFoundCityException()
            picnics = picnic_cursor.fetchall()
            if not picnics:
                raise exceptions.NotFoundPicnicException()

        return PicnicSerializer().serialize_picnic(picnics)
