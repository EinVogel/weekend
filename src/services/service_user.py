from sqlalchemy import insert, select
from typing import List, Optional

from src import exceptions
from src.schemas.schema_user import NewUserSchema, UserSchema
from src.services.service_base import BaseService
from src.models import User


class UserService(BaseService):
    """Сервис обработчик данных пользователя"""

    async def create_user(self, user: NewUserSchema) -> UserSchema:
        """Создание пользователя в базе"""
        try:
            session = self.db.get_session()
            async with session() as session:
                new_user_query = insert(User).values(user.dict()).returning(User.id,
                                                                            User.name,
                                                                            User.surname,
                                                                            User.age)
                user = await session.execute(new_user_query)
                await session.commit()
                user = user.fetchone()
                return UserSchema(id=user[0],
                                  name=user[1],
                                  surname=user[2],
                                  age=user[3])
        except BaseException:
            raise exceptions.CreateUserException()

    async def get_users(self,
                        name: Optional[str],
                        surname: Optional[str]) -> List[UserSchema]:
        """Получение пользователей из базы"""
        select_params = {}
        if name:
            select_params.update({"name": name.strip()})
        if surname:
            select_params.update({"surname": surname.strip()})
        session = self.db.get_session()
        async with session() as session:
            select_user_query = select(User).filter_by(**select_params)
            user_cursor = await session.execute(select_user_query)
            users = user_cursor.fetchall()
            if not users:
                raise exceptions.NotFoundUsersException()

        return [UserSchema(**user[0].__dict__) for user in users]
