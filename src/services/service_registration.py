from sqlalchemy import insert
from sqlalchemy.exc import IntegrityError

from src import exceptions
from src.schemas.schema_registration import NewRegistrationSchema, RegistrationSchema
from src.services.service_base import BaseService
from src.models import Registration


class RegistrationService(BaseService):
    """Сервис обработчик регистраций на пикник"""

    async def create_registration(self, registration: NewRegistrationSchema) -> RegistrationSchema:
        """Создание объекта регистрации в базе данных"""
        try:
            session = self.db.get_session()
            async with session() as session:
                new_user_query = insert(Registration) \
                    .values(registration.dict()).returning(Registration.id,
                                                           Registration.user_id,
                                                           Registration.picnic_id)
                registration_cursor = await session.execute(new_user_query)
                await session.commit()
                registration = registration_cursor.fetchone()
                return RegistrationSchema(id=registration[0],
                                          user_id=registration[1],
                                          picnic_id=registration[2])
        except IntegrityError:
            raise exceptions.ExistRegistrationException()
        except BaseException:
            raise exceptions.CreateRegistrationException()

