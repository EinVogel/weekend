import urllib.parse
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import declarative_base
from enum import Enum

Base = declarative_base()


class PostgresConnector:
    _autocommit_engine = None  # Движок для операций чтения, не требующих изоляции
    _read_committed_engine = None  # Движок для транзакций с уровнем изоляции READ COMMITTED
    _repeatable_read_engine = None  # Движок для транзакций с уровнем изоляции REPEATABLE READ
    _serializable_engine = None  # Движок для транзакций с уровнем изоляции SERIALIZABLE
    _isolate_level_map = None

    class IsolateTransactionTypes(Enum):
        autocommit = "AUTOCOMMIT"
        read_committed = "READ COMMITTED"
        repeatable_read = "REPEATABLE READ"
        serializable = "SERIALIZABLE"

    def connect(self,
                user: str,
                password: str,
                host: str,
                port: str,
                database_name: str,
                read_committed_engine: bool = False,
                repeatable_read_engine: bool = False,
                serializable_engine: bool = False,
                ):
        """Создание соединения с базой"""
        decoded_password = urllib.parse.quote(password)
        db_url = f"postgresql+asyncpg://{user}:{decoded_password}@{host}:{port}/{database_name}"
        self._autocommit_engine = create_async_engine(
            db_url, pool_pre_ping=True,
            isolation_level=PostgresConnector.IsolateTransactionTypes.autocommit.value)
        if read_committed_engine:
            self._read_committed_engine = self._autocommit_engine.execution_options(
                isolation_level=PostgresConnector.IsolateTransactionTypes.read_committed.value)
        if repeatable_read_engine:
            self._repeatable_read_engine = self._autocommit_engine.execution_options(
                isolation_level=PostgresConnector.IsolateTransactionTypes.repeatable_read.value)
        if serializable_engine:
            self._serializable_engine = self._autocommit_engine.execution_options(
                isolation_level=PostgresConnector.IsolateTransactionTypes.serializable.value)
        self._isolate_level_map = {
            self.IsolateTransactionTypes.autocommit.value: self._autocommit_engine,
            self.IsolateTransactionTypes.read_committed.value: self._read_committed_engine,
            self.IsolateTransactionTypes.repeatable_read.value: self._repeatable_read_engine,
            self.IsolateTransactionTypes.serializable.value: self._serializable_engine,
        }

    def __new__(cls):
        """Синглтон для получения объекта коннектора"""
        if not hasattr(cls, 'instance'):
            cls.instance = super(PostgresConnector, cls).__new__(cls)
        return cls.instance

    def get_session(self, isolation_level=IsolateTransactionTypes.autocommit.value):
        """Получение сессии"""
        # По умолчанию уровень изоляции autocommit
        session = sessionmaker(
            bind=self._isolate_level_map[isolation_level],
            class_=AsyncSession)
        return session

    async def disconnect(self):
        """Разрыв соединения с базой"""
        await self._autocommit_engine.dispose()
        if self._read_committed_engine:
            await self._read_committed_engine.dispose()
        if self._repeatable_read_engine:
            await self._repeatable_read_engine.dispose()
        if self._serializable_engine:
            await self._serializable_engine.dispose()

