import json
from typing import Optional
from aiokafka import AIOKafkaProducer, AIOKafkaConsumer


class KafkaConnector:
    _producer_pool: dict = {}
    _consumer_pool: dict = {}
    @staticmethod
    def create_configs(server: str, gssapi: bool = None) -> dict:
        """Создание настройки подключения"""
        cfg = {"bootstrap_servers": server}
        if gssapi:
            cfg.update(
                {"security_protocol": "SASL_PLAINTEXT",
                 "sasl_mechanism": "GSSAPI"}
            )
        return cfg
    @classmethod
    async def producer_connect(cls,
                               producer_name: str,
                               server: str,
                               gssapi: bool = None):
        """Подключение к продюсеру"""
        config = KafkaConnector.create_configs(server, gssapi)
        cls._producer_pool[producer_name] = AIOKafkaProducer(**config)
        await cls._producer_pool[producer_name].start()
    @classmethod
    async def consumer_connect(cls,
                               consumer_name: str,
                               listen_topic: str,
                               server: str,
                               gssapi: bool = None):
        """Подключение к консумену"""
        config = KafkaConnector.create_configs(server, gssapi)
        cls._consumer_pool[consumer_name] = AIOKafkaConsumer(listen_topic, **config)
        await cls._consumer_pool[consumer_name].start()
    @classmethod
    async def disconnect(cls):
        """Разрыв соединений"""
        for producer in cls._producer_pool.values():
            await producer.stop()
        for consumer in cls._consumer_pool.values():
            await consumer.stop()
    @classmethod
    async def send(cls,
                   producer_name: str,
                   topic: str,
                   message: dict,
                   headers: Optional[list] = None):
        """Отправка сообщений"""
        if not headers:
            headers = []
        await cls._producer_pool[producer_name].send(
            topic=topic,
            value=json.dumps(message).encode(),
            headers=headers,
        )
    @classmethod
    async def receive(cls, consumer_name: str):
        """Получение сообщений"""
        try:
            async for msg in cls._consumer_pool[consumer_name]:
                yield msg
        finally:
            await cls._consumer_pool[consumer_name].stop()


# Прослушивание сообщений
async def listen_message():
    async for message in KafkaConnector.receive(consumer_name="test"):
        print(message)


import asyncio
async def main():
    await KafkaConnector.producer_connect(producer_name="test",
                                          server="tst-etlkfk01.x5.ru:9092",
                                          gssapi=True)
    await KafkaConnector.consumer_connect(consumer_name="test",
                                          listen_topic="test.omni.bot.web.out",
                                          server="tst-etlkfk01.x5.ru:9092",
                                          gssapi=True)
    await KafkaConnector.send(producer_name="test",
                              topic="test.omni.bot.web.out",
                              message={"text": "hello"})
    asyncio.create_task(listen_message())
    await asyncio.sleep(5)
    await KafkaConnector.disconnect()


asyncio.run(main())
