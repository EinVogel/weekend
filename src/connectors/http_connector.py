import ssl
from typing import Optional, Union
from aiohttp import ClientResponse, ClientSession


class AsyncHttpConnector:
    _session_pool: dict = None

    def create_session(self, name: str):
        """Создание сеанса"""
        if not self._session_pool:
            self._session_pool = {}
        self._session_pool[name] = ClientSession()

    def __new__(cls):
        """Синглтон для получения объекта коннектора"""
        if not hasattr(cls, 'instance'):
            cls.instance = super(AsyncHttpConnector, cls).__new__(cls)
        return cls.instance

    async def close_session(self, name: str):
        if not self._session_pool.get(name):
            return
        await self._session_pool[name].close()
        del self._session_pool[name]

    async def close_all_sessions(self):
        for session_name, session in self._session_pool.items():
            await session.close()
        self._session_pool = {}

    async def send_request(self,
                           method: str,
                           session_name: str,
                           url: str,
                           params: Optional[dict] = None,
                           json: Optional[Union[dict, list]] = None,
                           headers: Optional[dict] = None,
                           timeout: Optional[int] = None,
                           ssl_verify: Optional[bool] = None,
                           ssl_file_path: Optional[bool] = None,
                           ssl_context: Optional[bool] = None,
                           ) -> Optional[ClientResponse]:
        if ssl_verify:
            ssl_context = ssl.create_default_context(cafile=ssl_file_path)
        """Отправка асинхронного запроса."""
        response = await self._session_pool[session_name].request(method=method,
                                                                  url=url,
                                                                  params=params,
                                                                  headers=headers,
                                                                  timeout=timeout,
                                                                  json=json,
                                                                  verify_ssl=ssl_verify,
                                                                  ssl_context=ssl_context)
        return response
