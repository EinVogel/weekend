# Импорт для миграций
from src.connectors.postgres_connector import Base
from src.models.model_city import City
from src.models.model_picnic import Picnic
from src.models.model_registration import Registration
from src.models.model_user import User
