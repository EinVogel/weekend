from sqlalchemy import Column, String, Integer

from src.config import PostgresConfig
from src.connectors.postgres_connector import Base


class User(Base):
    """Модель пользователя"""

    __tablename__ = PostgresConfig.USER_TABLE_NAME

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    surname = Column(String, nullable=False)
    age = Column(Integer, nullable=True)

