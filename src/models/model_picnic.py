from sqlalchemy import Column, Integer, ForeignKey, DateTime

from src.config import PostgresConfig
from src.connectors.postgres_connector import Base


class Picnic(Base):
    """Модель пикника"""

    __tablename__ = PostgresConfig.PICNIC_TABLE_NAME

    id = Column(Integer, primary_key=True, autoincrement=True)
    city_id = Column(Integer,
                     ForeignKey(f"{PostgresConfig.CITY_TABLE_NAME}.id", ondelete="CASCADE"),
                     nullable=False)
    time = Column(DateTime(timezone=True), nullable=False)
