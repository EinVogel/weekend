from sqlalchemy import Column, Integer, String

from src.config import PostgresConfig
from src.connectors.postgres_connector import Base


class City(Base):
    """Модель города"""

    __tablename__ = PostgresConfig.CITY_TABLE_NAME

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True, nullable=False)

