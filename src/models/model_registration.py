from sqlalchemy import Column, Integer, ForeignKey, UniqueConstraint

from src.config import PostgresConfig
from src.connectors.postgres_connector import Base


class Registration(Base):
    """Модель регистрации"""

    __tablename__ = PostgresConfig.REGISTRATION_TABLE_NAME

    __table_args__ = (
        UniqueConstraint('user_id', 'picnic_id', name='uniq_user_picnic'),
    )

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer,
                     ForeignKey(f"{PostgresConfig.USER_TABLE_NAME}.id", ondelete="CASCADE"),
                     nullable=False)
    picnic_id = Column(Integer,
                       ForeignKey(f"{PostgresConfig.PICNIC_TABLE_NAME}.id", ondelete="CASCADE"),
                       nullable=False)
    # Уникальное соотношение пользователя и пикника,
    # чтобы не засорять базу одинаковыми регистрациями
    # UniqueConstraint('user_id', 'picnic_id', name='unique_user_picnic')


