import os
from envparse import Env
from logging.config import fileConfig
from sqlalchemy import create_engine
from alembic import context

env = Env()
# Для локального создания миграций
if os.path.exists('weekend_local_settings.env'):
    env.read_envfile('weekend_local_settings.env')

from src.config import PostgresConfig
from src.models import Base

target_metadata = Base.metadata
config = context.config
fileConfig(config.config_file_name)


def get_db_url():
    return f'postgresql://{PostgresConfig.PG_USER}:{PostgresConfig.PG_PASSWORD}@' \
           f'{PostgresConfig.PG_HOST}:{PostgresConfig.PG_PORT}/{PostgresConfig.PG_DATABASE_NAME}'


def run_migrations(db_url, metadata):
    connectable = create_engine(db_url.__call__())

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=metadata
        )

        with context.begin_transaction():
            context.run_migrations()


run_migrations(get_db_url, target_metadata)
