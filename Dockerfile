FROM python:3.10.11
RUN mkdir app
WORKDIR app
COPY /src/ /app/src
COPY /tests/ /app/tests
RUN pip install poetry
COPY /poetry.lock/ /app/
COPY /pyproject.toml/ /app/
COPY /.coveragerc/ /app/
COPY /alembic.ini/ /app/
COPY /main.py/ /app/
RUN poetry install
CMD poetry run uvicorn main:app --host 0.0.0.0 --port 8000
