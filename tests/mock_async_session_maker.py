class AsyncSessionMakerMock:
    """Мок асинхронной менеджера контекста асинхронной сессии"""

    async_session_mock = None

    async def __aenter__(self):
        return self.async_session_mock()

    async def __aexit__(self, exc_type, exc, tb):
        self.async_session_mock.return_data = None
