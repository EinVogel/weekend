class PostgresCursorMock:
    """Мок указателя к данным"""

    def __init__(self, fetchone_data=None, fetchall_data=None):
        self.fetchone_data = fetchone_data
        self.fetchall_data = fetchall_data

    def fetchone(self):
        return self.fetchone_data

    def fetchall(self):
        return self.fetchall_data
