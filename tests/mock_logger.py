class LoggerMock:
    """Мок асинхронного логгера"""

    return_data = None

    async def info(self, message):
        """Обработка сообщений с уровнем логгирования info"""
        pass

    async def error(self, message):
        """Обработка сообщений с уровнем логгирования error"""
        pass

    async def debug(self, message):
        """Обработка сообщений с уровнем логгирования debug"""
        pass
