import os
import pytest
from fastapi.testclient import TestClient
from unittest.mock import MagicMock

from tests.mock_postgres_connector import PostgresConnectorMock
from tests.mock_async_session_maker import AsyncSessionMakerMock
from tests.mock_async_session import AsyncSessionMock
from tests.mock_postgres_cursor import PostgresCursorMock
from tests.mock_logger import LoggerMock


def set_mock_env_vars():
    """Проброс переменных"""
    os.environ['WEATHER_TIMEOUT'] = '3'
    os.environ['CITY_TABLE_NAME'] = 'city'
    os.environ['PICNIC_TABLE_NAME'] = 'picnic'
    os.environ['REGISTRATION_TABLE_NAME'] = 'registration'
    os.environ['USER_TABLE_NAME'] = 'user'


@pytest.fixture(scope="module")
def db():
    """Получение мока базы данных"""
    async_session_mock = AsyncSessionMock
    async_session_maker = AsyncSessionMakerMock
    async_session_maker.async_session_mock = async_session_mock
    db = PostgresConnectorMock(async_session_maker)
    return db


@pytest.fixture(scope="module")
def cursor():
    """Получение мока курсора базы"""
    return PostgresCursorMock()


@pytest.fixture(scope="module")
def client(db):
    """Получение фикстуры клиента"""
    set_mock_env_vars()
    from main import app
    app.state._db = db
    app.state._http = MagicMock()
    app.state._logger = LoggerMock()
    return TestClient(app)










