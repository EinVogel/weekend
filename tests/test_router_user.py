from fastapi.testclient import TestClient

from src.schemas.schema_user import NewUserSchema, UserSchema


def test_create_user_success(client: TestClient, cursor):
    """Тест на успешное создание пользователя"""

    new_user = NewUserSchema(name='John', surname='Doe', age=30)

    cursor.fetchone_data = [1, 'John', 'Doe', 30]
    client.app.state._db \
        .async_session_maker_mock \
        .async_session_mock \
        .set_data_for_return('execute', cursor)

    response = client.post('/user', json=new_user.dict())

    assert response.status_code == 201
    assert response.json() == UserSchema(**new_user.dict(), id=1).dict()


def test_create_user_fail(client: TestClient, cursor):
    """Тест на неуспешное создание пользователя"""

    new_user = NewUserSchema(name='John', surname='Doe', age=30)

    # симуляция ошибки при создании пользователя, случай,
    # когда база не вернула результат создания
    cursor.fetchone_data = []
    client.app.state._db \
        .async_session_maker_mock \
        .async_session_mock \
        .set_data_for_return('execute', cursor)

    response = client.post('/user', json=new_user.dict())

    assert response.status_code == 400
    assert response.json() == {'message': 'Ошибка при регистрации пользователя',
                               'status': 'CREATE_USER_ERROR'}


def test_get_user_success(client: TestClient, cursor):
    """Тест на успешное получение списка пользователей"""

    cursor.fetchall_data = [(UserSchema(id=1, name='John', surname='Doe', age=20),),
                            (UserSchema(id=2, name='Mike', surname='Fisher', age=21),)]
    client.app.state._db \
        .async_session_maker_mock \
        .async_session_mock \
        .set_data_for_return('execute', cursor)

    response = client.get('/user')

    assert response.status_code == 200
    assert response.json() == [{'id': 1, 'name': 'John', 'surname': 'Doe', 'age': 20},
                               {'id': 2, 'name': 'Mike', 'surname': 'Fisher', 'age': 21}]


def test_get_user_fail(client: TestClient, cursor):
    """Тест на неуспешное получение списка пользователей"""

    cursor.fetchall_data = []
    client.app.state._db \
        .async_session_maker_mock \
        .async_session_mock \
        .set_data_for_return('execute', cursor)

    response = client.get('/user')

    assert response.status_code == 404
    assert response.json() == {'status': 'NOT_FOUND_USER_ERROR',
                               'message': 'Не найдено в базе пользователей'
                                          ' с запрашиваемыми идентификаторами'}


def test_get_user_with_param_name_success(client: TestClient, cursor):
    """Тест на успешное получение пользователя по имени"""

    cursor.fetchall_data = [(UserSchema(id=2, name='Mike', surname='Fisher', age=21),)]
    client.app.state._db \
        .async_session_maker_mock \
        .async_session_mock \
        .set_data_for_return('execute', cursor)

    response = client.get('/user', params={'name': 'Mike'})

    assert response.status_code == 200
    assert response.json() == [{'id': 2, 'name': 'Mike', 'surname': 'Fisher', 'age': 21}]


def test_get_user_with_param_surname_success(client: TestClient, cursor):
    """Тест на успешное получение пользователя по фамилии"""

    cursor.fetchall_data = [(UserSchema(id=2, name='Mike', surname='Fisher', age=21),)]
    client.app.state._db \
        .async_session_maker_mock \
        .async_session_mock \
        .set_data_for_return('execute', cursor)

    response = client.get('/user', params={'surname': 'Fisher'})

    assert response.status_code == 200
    assert response.json() == [{'id': 2, 'name': 'Mike', 'surname': 'Fisher', 'age': 21}]
