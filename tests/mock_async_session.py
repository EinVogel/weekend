class AsyncSessionMock:
    """Мок асинхронной сессии"""

    return_data = None

    @classmethod
    def set_data_for_return(cls, method, value):
        """Установка значений для возвращаемой функции"""
        if not cls.return_data:
            cls.return_data = {}
        cls.return_data.update({method: value})

    async def execute(self, query):
        """Метод имитирующий ответ от базы данных"""
        return self.return_data["execute"]

    async def commit(self):
        """Мок коммита в базу"""
        pass
