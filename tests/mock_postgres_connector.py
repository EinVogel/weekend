class PostgresConnectorMock:
    """Мок соединения к базе данных"""
    
    def __init__(self, async_session_maker_mock):
        self.async_session_maker_mock = async_session_maker_mock
        
    def get_session(self):
        """Получение мока асинхронной сессии"""
        return self.async_session_maker_mock
