import pytest
from pydantic import ValidationError

from src.schemas.schema_user import NewUserSchema, UserSchema


def test_create_new_user_schema():
    """Тест на создание экземпляра класса NewUserSchema"""

    new_user = NewUserSchema(name='John', surname='Doe', age=30)

    assert new_user.name == 'John'
    assert new_user.surname == 'Doe'
    assert new_user.age == 30


def test_invalid_new_user_schema():
    """Тест на передачу некорректных данных в экземпляр класса NewUserSchema"""

    with pytest.raises(ValidationError):
        NewUserSchema(name='John', surname='Doe', age=[30])


def test_create_user_schema():
    """Тест на создание экземпляра класса UserSchema"""

    user = UserSchema(id=1, name='John', surname='Doe', age=30)

    assert user.id == 1
    assert user.name == 'John'
    assert user.surname == 'Doe'
    assert user.age == 30


def test_invalid_user_schema():
    """Тест на передачу некорректных данных в экземпляр класса NewUserSchema"""

    with pytest.raises(ValidationError):
        UserSchema(id=1, name='John', surname='Doe', age=[30])

